<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">Etat de la station</th>
      <th scope="col">Code de la station</th>
      <th scope="col">Nom de la station</th>
      <th scope="col">Ville</th>
      <th scope="col">Nom du cours d'eau</th>
      <th scope="col">Date de la derniere mise à jour</th>
    </tr>
  </thead>
  <tbody>

    <?php foreach ($json_data as $datas): ?>
      <?php if (is_array($datas)): ?>
        <?php foreach($datas as $data): ?>
          <tr>
            <th scope="row" class="etat"><span class="<?= $data['en_service'] ? 'true' : 'false' ?>">●</span></th>
            <td><?= $data['code_station'] ?></td>
            <td><?= $data['libelle_station'] ?></td>
            <td><?= $data['libelle_commune'] ?></td>
            <td><?= $data['libelle_cours_eau'] ?></td>
            <td><?= datefr($data['date_maj_station']) ?></td>
          </tr>
        <?php endforeach; ?>
      <?php endif; ?>
    <?php endforeach; ?>

  </tbody>
</table>
