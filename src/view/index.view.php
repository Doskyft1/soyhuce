<?php ob_start() ?>

  <nav>
    <div class="toggle">
      <a href="?bookmark">Afficher les favoris</a>
    </div>

    <div class="toggle toggleFilter">
      <p>Afficher les filtres</p>
    </div>
  </nav>

  <form class="filter" action="index.php" method="get" style="display:none">
    <div class="form-group row">
      <label for="code_departement" class="col-4 col-form-label">Numero du département</label>
      <div class="col-8">
        <input type="number" class="form-control" name="code_departement" value="<?= isset($_GET['code_departement']) ? $_GET['code_departement'] : "" ?>" min="1">
      </div>
    </div>

    <div class="custom-control custom-radio">
      <input type="radio" id="StationOk" name="en_service" value="true" class="custom-control-input" <?= (isset($_GET['en_service']) && $_GET['en_service'] == "StationOk") ? "checked" : "" ?> >
      <label class="custom-control-label" for="StationOk">Station en service</label>
    </div>
    <div class="custom-control custom-radio">
      <input type="radio" id="StationOff" name="en_service" value="false" class="custom-control-input" <?= (isset($_GET['en_service']) && $_GET['en_service'] == "StationOff") ? "checked" : "" ?> >
      <label class="custom-control-label" for="StationOff">Station hors service</label>
    </div>
    <div class="custom-control custom-radio">
      <input type="radio" id="StationOkAndOff" name="en_service" value="null" class="custom-control-input" <?= (isset($_GET['en_service']) && $_GET['en_service'] == "StationOkAndOff" || !isset($_GET['en_service'])) ? "checked" : "" ?> >
      <label class="custom-control-label" for="StationOkAndOff">Peu importe</label>
    </div>
    <button class="btn btn-primary" type="submit">Valider les filtres</button>
  </form>

  <?php require_once 'table.template.php'; ?>

  <script src="js/filter.js" charset="utf-8" defer></script>

<?php $content = ob_get_clean();
