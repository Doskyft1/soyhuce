<?php

function datefr($date) {
  $date = substr($date, 0, 10);
  list($annee, $mois, $jour) = explode('-', $date);
  return $jour . "/" . $mois . "/" . $annee;
}

$apiLink = "https://hubeau.eaufrance.fr/api/v1/hydrometrie/referentiel/stations?";

if (isset($_GET['bookmark'])) {

  $json_data = [];
  $bookmark = file("bookmark");
  foreach ($bookmark as $code_station) {
    $json_data[] = json_decode(file_get_contents($apiLink . "code_station=" . trim($code_station)), true)["data"];
  }

  require_once 'view/bookmark.view.php';
} else {
  if (!empty($_GET['code_departement'])) {
     $apiLink .= "code_departement=" . ($_GET['code_departement'] < 10 ? "0" . $_GET['code_departement'] : $_GET['code_departement']);
   }

  if (!empty($_GET['en_service']) && $_GET['en_service'] != null) {
    $apiLink .= "&en_service=" . $_GET['en_service'];
  }

  $json_source = file_get_contents($apiLink);
  $json_data = json_decode($json_source, true);

  require_once 'view/index.view.php';
}



require_once 'view/template.php';
