document.querySelector(".toggleFilter").addEventListener("click", function(e) {
  const form = document.querySelector("form.filter")
  if (form.style.display == "none") {
    form.style.display = "block"
    e.target.innerText = "Cacher les filtres"
  }
  else {
    form.style.display = "none"
    e.target.innerText = "Afficher les filtres"
  }
})
